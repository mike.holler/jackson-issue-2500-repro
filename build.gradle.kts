import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.41"
    `java-library`
}

val jacksonVersion = project.findProperty("jacksonVersion") ?: "2.10.0"

val javaVersion by extra { JavaVersion.VERSION_11 }
val junitVersion by extra { "5.5.2" }

java {
    sourceCompatibility = javaVersion
    targetCompatibility = javaVersion
}

repositories {
    jcenter()
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation(platform("com.fasterxml.jackson:jackson-bom:$jacksonVersion"))
    implementation("com.fasterxml.jackson.core:jackson-databind")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    testImplementation("io.strikt:strikt-core:0.22.2")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}

tasks {
    test {
        useJUnitPlatform()
    }
    withType<KotlinCompile>().all {
        kotlinOptions.jvmTarget = javaVersion.toString()
    }
}
