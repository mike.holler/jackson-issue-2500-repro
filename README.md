# Jackson Issue #2500 Repro Project

This project attempts to reproduce the issue from https://github.com/FasterXML/jackson-databind/issues/2500.

When writing this code in a new, isolated project, I learned about BOMs (which I was not using before) and was able to write tests that reproduced the issue with Jackson version `2.9.10`.

Encouraged, I updated to `2.10.0` just to make sure it was present on the most recent version, which from combing the commit logs and merge requests, seemed like it still would be.
However, when I ran the tests with that version, I got the expected behavior I desired, instead of the failures I got using `2.9.10`.
So, this isn't an issue anymore.

To test this out yourself, run the following command to reproduce the failure I was seeing:

```bash
./gradlew check -PjacksonVersion=2.9.10
```

And then run this command to see the success that I ultimately found:

```bash
./gradlew check -PjacksonVersion=2.10.0
```

I'd still be really curious to know what MR fixed this issue, but I have no clue how to track that down.
