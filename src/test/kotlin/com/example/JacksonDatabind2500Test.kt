package com.example

import com.fasterxml.jackson.annotation.JsonSetter
import com.fasterxml.jackson.annotation.Nulls
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isEqualTo

class JacksonDatabind2500Test {

    private lateinit var mapper: ObjectMapper
    private val jsonContainingNulls = """ { "strings": ["a", null, "c"] } """
    private val jsonNotContainingNulls = """ { "strings": ["a", "b", "c"] } """

    @BeforeEach
    fun setUp() {
        mapper = ObjectMapper().registerKotlinModule()
    }

    data class ListOfNotNullWithJsonSetterDefaultAnnotationSite(
        @JsonSetter(contentNulls = Nulls.FAIL)
        val strings: List<String>
    )

    @Test
    fun `ListOfNotNullWithJsonSetterDefaultAnnotationSite deserialization fails when list contains null`() {
        expectThrows<JsonProcessingException> {
            mapper.readValue<ListOfNotNullWithJsonSetterDefaultAnnotationSite>(jsonContainingNulls)
        }
    }

    @Test
    fun `ListOfNotNullWithJsonSetterDefaultAnnotationSite deserialization succeeds when list does not contain null`() {
        expectThat(mapper.readValue<ListOfNotNullWithJsonSetterDefaultAnnotationSite>(jsonNotContainingNulls))
            .isEqualTo(ListOfNotNullWithJsonSetterDefaultAnnotationSite(listOf("a", "b", "c")))
    }
}
